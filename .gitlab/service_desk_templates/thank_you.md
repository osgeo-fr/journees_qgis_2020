Bonjour, 
Merci de nous avoir contacté pour les journées QGIS utilisateurs des 10, 11 et 12 juin 2025 en Avignon. 


Votre demande a été enregistrée sous le numéro %{ISSUE_ID}.

Nous allons revenir vers vous dans les plus brefs délais. 

Bien cordialement,

L'équipe d'organisation des journées QGIS FR


_Retrouvez toutes les informations sur notre site  https://conf.qgis.osgeo.fr/ et sur https://mapstodon.space/tags/QGISUsersDaysFR_


