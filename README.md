# Projet dédié à l'organisation des journées QGIS

Le séminaire utilisateurs **QGIS-fr 2025** se déroulera en Avignon. Cet événement est organisé par l’OSGeo-fr en partenariat avec Avignon Université.

L’événement se déroulera sur 3 jours, les 10,11 et 12 juin 2025:

    Mardi 10 juin : Journée d’ateliers thématiques et techniques.

    Mercredi 11 juin : Journée de conférences,

    Jeudi 12 juin : journée de contribution / code sprint.


## LIENS ET INFORMATIONS UTILES

* Organisation et tâches à réaliser pour les réunions :
  * PENDANT la réunion (référent : @All) :
    * **Rédiger le compte-rendu** (via framapad) en complétant l'ODJ avec les réponses obtenues et/ou décisions prises pendant la réunion
    * chacun note son nom dans la liste des présents ds le CR
    * **_Eviter de recopier les informations déjà présentes dans les tickets (ou via leurs labels)_**
  * APRES la réunion (référent : @roelandtn) :
    * **Recopier le compte-rendu** ***en remplacement*** de la **description** du ticket de la réunion. Ticket à renommer en enlevant le mot "prochaine"
    * **Créer un nouveau ticket** avec la date de la prochaine réunion, en reprenant les points qui seront à revoir selon le CR de la réunion passée. *Ne pas reporter les points déjà traités*.
    * **Créer le framapad de la prochaine réunion** et mettre à jour le lien dans le ticket de la prochaine réunion
  * AVANT la prochaine réunion (référent : @GlaDal) :
    * J-15 à J-5 : **Alimenter** l'ODJ sur l'issue de la réunion (Gitlab). Chaque référent de comité fait le point sur les sujets à partager et/ou à décider
    * J-3 : **Envoyer** rappel aux référents de comité pour alimenter l'ODJ(viacommentaire de l'issue de la réunion)
    * J-1 :
       - **Vérifier** que l'ODJ est prêt et copier l'ODJ sur le framapad
       - **Diffuser** rappel de la réunion (via mail sur QgisConf)
    * J0 : **Partager** le lien du framapad (via mail sur QgisConf) en début de réunion pour ceux qui ne seraient pas venus sur le ticket de la réunion pour voir le lien.

